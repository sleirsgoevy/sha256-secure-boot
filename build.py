import sys, os.path
sys.path.append(os.path.split(__file__)[0]+'/../refimpl')
import sha256

data = open('impl', 'rb').read()

ans = b''

for l in open('map.txt'):
    if (l+' ').isspace(): continue
    a, b = eval(l)
    if b == None:
        assert a <= len(data)
        ans += data[a:]
    elif a == None:
        ans += bytes(b)
    else:
        assert a + b <= len(data)
        ans += data[a:a+b]

for l in reversed(list(open('hashes.txt'))):
    if (l+' ').isspace(): continue
    data = eval(l)
    if isinstance(data[0], int): data = (data,)
    chunk = b''
    subtract = False
    for a, b in data:
        if b != None and b < 0:
            b = -b
            subtract = True
        if b != None:
            assert a + b <= len(ans)
            chunk += ans[a:a+b]
        else:
            assert a <= len(ans)
            chunk += ans[a:]
    hsh = sha256.do_hash(chunk, consts='mod', pad=False, endian='little', subtract=subtract)
    ans2 = ans[::-1].replace(b'\xb3'*32, hsh, 1)[::-1]
    assert ans2 != ans
    ans = ans2

assert (b'\xb3'*32) not in ans

with open('hdd.img', 'wb') as file: file.write(ans)
