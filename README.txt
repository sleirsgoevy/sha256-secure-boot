# Legacy BIOS secure boot

Boots embedded kernel/initrd pair. Root of trust is in bootsector (modified SHA-256 implementation + hash chain). In other words, if bootsector is untampered only the bundled kernel/initrd can boot.
