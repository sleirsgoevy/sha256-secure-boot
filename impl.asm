section .text
org 0x7c00
use16

_begin:
; generic init
nop
jmp 0:start

vars: ; stored in reverse order, to make rotating easier
dd 0xab1c5ed5
dd 0x923f82a4
dd 0x59f111f1
dd 0x3956c25b
dd 0xe9b5dba5
dd 0xb5c0fbcf
dd 0x71374491
dd 0x428a2f98

right_hash:
times 32 db 0xb3

start:
mov ax, cs
mov ds, ax
mov es, ax
mov ss, ax
mov esp, 0x8000

; save DL
push dx

; read data sector
mov dh, 0
mov cx, 2
mov ax, 0x0202
mov bx, 0x8200
int 0x13

; fpu init
mov eax, cr0
and al, ~4
mov cr0, eax
finit
fldcw [control_word]

; compute constants
mov di, 0x8000
mov cx, 64
mov ax, 2

consts_loop:
push ax
mov bx, sp
fild word [bx]
fsqrt
fld st0
frndint
fsubp

sub sp, 10

xor ebx, ebx
mov bx, sp
fstpt [bx]
add word [bx+8], 32
fldt [bx]
fistp qword [bx]
pop eax
stosd
add sp, 6
pop ax

next_prime:
inc ax
mov bx, 2
is_prime:
push ax
xor dx, dx
idiv bx
pop ax
test dx, dx
jz next_prime
inc bx
cmp bx, ax
jnz is_prime

loop consts_loop

; run sha256

; copy source data
mov si, 0x8200
sha256_iter:
mov di, bss_words
mov cx, 64
rep movsb

; generate 48 words
mov cx, 48
gen_words:
mov eax, [di-60]
mov ebx, 0x030e0b07
call xorrotshr
push eax
mov eax, [di-8]
mov ebx, 0x0a0d0211
call xorrotshr
pop ebx
add eax, ebx
add eax, [di-64]
add eax, [di-28]
stosd
loop gen_words

; copy vars to bss
mov si, vars
mov di, bss_vars
mov cx, 32
rep movsb

; sha256 main loop
mov si, bss_words
mov di, 0x8000 ; constants, see above
mov cx, 64
mov bp, bss_vars

main_loop:
push cx

; sigma0
mov eax, [bp+28] ; a
push eax
mov ebx, 0x00090b02
call xorrotshr
pop ebx
push eax

; Ma
mov eax, [bp+24] ; b
mov ecx, [bp+20] ; c
mov edx, eax
and edx, ebx
and eax, ecx
and ebx, ecx
xor eax, ebx
xor eax, edx

; t2
pop ebx
add ebx, eax
push ebx

; sigma1
mov eax, [bp+12] ; e
push eax
mov ebx, 0x000e0506
call xorrotshr
pop ebx
push eax

; Ch
mov eax, [bp+8] ; f
mov ecx, [bp+4] ; g
and eax, ebx
not ebx
and ecx, ebx
xor eax, ecx

; t1
pop ebx
add ebx, eax
add ebx, [bp] ; h
lodsd
add eax, ebx
add eax, [di]
add di, 4
push eax

; rotate vars
push si
push di
mov di, bp
lea si, [di+4]
mov cx, 28
rep movsb
pop di
pop si

; e += t1
pop eax
add [bp+12], eax

; a = t1 + t2
pop ebx
add eax, ebx
mov [bp+28], eax

; continue main loop
pop cx
sub cx, 1
jnz main_loop

; will later become a function
sha256_ret_here:
;nop

; compare the hash against the authentic value
mov di, right_hash
validate:
mov si, bss_vars
validate2:
mov cx, 32
validate_loop:
lodsb
mov bl, [di]
inc di
cmp al, bl
validation_hang:
jnz validation_hang
loop validate_loop

; will also become a function
validate_ret_here:
jmp 0x8200

; eax = operand
; ebx = operation
;  bl = first
;  bh = second - first
;  ebl = (third - second) || (32 - second)
;  ebh = (32) || (third)
;; TODO: probably suboptimal
xorrotshr:
push ecx
;push bp
;mov bp, xorrotshr
;mov [bp+xrs_1+3-xorrotshr], bl
;mov [bp+xrs_2+3-xorrotshr], bh
;ror ebx, 16
;mov [bp+xrs_3+3-xorrotshr], bl
;mov [bp+xrs_4+3-xorrotshr], bh
xor edx, edx
xrs_1:
mov cl, bl
ror eax, cl ;0
;mbr part1 type = 0
db 0xeb, 0x02, 0x00, 0x00
xor edx, eax
xrs_2:
mov cl, bh
ror eax, cl ;0
xor edx, eax
;mbr part2 type = 0
db 0xeb, 0x03, 0x00, 0x00, 0x00
ror ebx, 16 ;new
xrs_3:
mov cl, bl
ror eax, cl ;0
xrs_4:
mov cl, bh
shr eax, cl ;0
;mbr part3 type = 0
db 0xeb, 0x00
xor eax, edx
;pop bp
pop ecx
ret

control_word:
dw 0x77f
end:
times 510-end+_begin db 0
db 0x55, 0xaa
bss_words:
times 256 db 0
bss_vars:
times 32 db 0

; sector 2 must be at 0x8200
pad_sector_2:
times 1536+_begin-pad_sector_2 db 0

sector_2:

; patch sector 1
mov al, 0xc3
mov [sha256_ret_here], al
mov [validate_ret_here], al

; hash part 2
mov si, sector_2+64
call sha256_iter

; validate the result
mov di, sector2_hash
call validate
jmp validate3

sector2_hash:
times 32 db 0xb3

; validate the rest
validate3:
mov si, sector_2+128
call sha256_iter
mov di, sector2_hash2
call validate
jmp validate4
sector2_hash2:
times 32 db 0xb3

validate4:
mov si, sector_2+192
call sha256_iter
mov di, sector2_hash3
call validate
jmp validate5
sector2_hash3:
times 32 db 0xb3

validate5:
mov si, sector_2+256
call sha256_iter
mov di, sector2_hash4
call validate
jmp validate6
sector2_hash4:
times 32 db 0xb3

validate6:
mov si, sector_2+320
call sha256_iter
mov di, sector2_hash5
call validate
jmp validate7
sector2_hash5:
times 32 db 0xb3

validate7:
mov si, sector_2+384
call sha256_iter
mov di, sector2_hash6
call validate
jmp validate8
sector2_hash6:
times 32 db 0xb3

validate8:
mov si, sector_2+448
call sha256_iter
mov di, sector2_hash7
call validate
jmp validate9
sector2_hash7:
times 32 db 0xb3

validate9:

; validate sector 3
mov si, 0x8400
mov cx, 8
call sha256_with_loop
mov di, sector2_hash8
mov si, vars
call validate2

; jump to sector 3
jmp 0x8400

; full sha256
;  si - source data
;  cx - number of blocks
sha256_with_loop:
call restore_vars
sha256_loop:
push si
push cx
call sha256_iter
call add_back
pop cx
pop si
add si, 64
loop sha256_loop
ret_ins:
ret

restore_vars:
push si
push cx
mov si, sector2_vars
mov di, vars
mov cx, 32
rep movsb
pop cx
pop si
ret

; add back routine
add_back:
mov si, bp
mov di, vars
mov cx, 8
add_back_loop:
lodsd
add eax, [di]
stosd
loop add_back_loop
ret

sector2_vars: ; same as in sector 1
dd 0xab1c5ed5
dd 0x923f82a4
dd 0x59f111f1
dd 0x3956c25b
dd 0xe9b5dba5
dd 0xb5c0fbcf
dd 0x71374491
dd 0x428a2f98

sector2_hash8:
times 32 db 0xb3

sector_2_end:
times 512+sector_2-sector_2_end db 0
sector_3:

; set video mode
mov eax, 3
int 0x10

; print stage1 message
mov si, stage1_ok
stage1_msg_loop:
lodsb
test al, al
jz stage1_msg_done
mov ah, 0x0e
int 0x10
jmp stage1_msg_loop
stage1_msg_done:

mov word [stage1_msg_done], 0xc3 ; ret

; load drive number
pop dx
push dx

; get drive geometry
mov ah, 8
xor di, di
int 0x13
inc dh
mov [heads], dh
mov dl, cl
and dl, 63
mov [sectors], dl
shr cl, 6
ror cx, 8
inc cx
mov [cylinders], cx

; unlock a20
; copy-pasted from osdev.org
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        jmp a20_after
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret

a20_after:

; enter unreal mode
lgdt [gdtr]
mov eax, cr0
or al, 1
mov cr0, eax
mov ax, 8
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
mov eax, cr0
and al, 254
mov cr0, eax
mov ax, 0
mov ds, ax
mov es, ax
mov ss, ax
mov fs, ax
mov gs, ax
sti

; reset vars for hashing
call restore_vars

; read payload from the disk
mov edi, 0x100000
mov eax, 3

; read the next sector
push edi
push eax
sector3_read_loop:
xor ebx, ebx
mov bx, [sectors]
xor edx, edx
idiv ebx
mov cx, dx
inc cx
mov bx, [heads]
xor edx, edx
idiv ebx
mov dh, dl
ror ax, 8
shl al, 6
or cx, ax
mov bp, sp
add bp, 8
mov dl, [bp]
mov ax, 0x0201
mov bx, 0x8600
clc
int 0x13
jnc int13_error_no_hang
int13_error_hang:
mov si, disc_read_error_msg
call stage1_msg_loop
db 0xeb, 0xfe
int13_error_no_hang:

; hash the sector
mov si, 0x8600
mov cx, 8
call sha256_loop

; increment sector number
pop eax
inc eax
push eax
xor edx, edx
mov ecx, 2048
div ecx
test edx, edx
jnz no_dot

; print dot
mov ax, 0x0e00 + '.'
int 0x10

no_dot:

; verify that the sector is nonzero
mov si, 0x8600
mov cx, 512

sector3_nonzero:
lodsb
cmp al, 0xb2
jnz sector3_nonzero_ok
loop sector3_nonzero

jmp sector3_all_zeros
sector3_nonzero_ok:

; copy to destination
pop eax
pop edi
mov esi, 0x8600
mov ecx, 512
rep A32 movsb
push edi
push eax

; continue reading
jmp sector3_read_loop

; finished reading
sector3_all_zeros:

; pop locals
add esp, 8

; verify final hash
mov di, sector3_hash
mov si, vars
call validate2

mov ax, 0x0e0d
int 0x10
mov ax, 0x0e0a
int 0x10

; switch to PM and boot
cli
mov eax, cr0
or al, 1
mov cr0, eax
jmp 16:goto_final
goto_final:
use32
jmp 0x100000

gdt:
db 0, 0, 0, 0, 0, 0, 0, 0
db 0xff, 0xff, 0, 0, 0, 0x92, 0xcf, 0
db 0xff, 0xff, 0, 0, 0, 0x9a, 0xcf, 0
db 0xff, 0xff, 0, 0, 0, 0x9a, 0x8f, 0

gdtr:
dw 31
dd gdt

cylinders:
db 0, 0
heads:
db 0, 0
sectors:
db 0, 0

stage1_ok:
db "stage1 ok", 13, 10, 0

disc_read_error_msg:
db 13, 10, "disc read error", 13, 10, 0

sector3_hash:
times 32 db 0xb3

sector_3_end:
times 512+sector_3-sector_3_end db 0
times 0x100000-0x8600 db 0
main_body:

use32

; embed binary blobs
jmp actual_body
kernel:
incbin "kernel"
initrd:
incbin "initrd.img"
cmdline:
db "nopassany", 0
actual_body:

; print "Loading the kernel..." message
mov esi, loading_msg
mov edi, 0xb8000
msg_loop:
lodsb
test al, al
jz msg_end
stosb
mov al, 7
stosb
jmp msg_loop
msg_end:

; load the real-mode kernel at 0x10000
mov esi, kernel
mov edi, 0x10000
xor ecx, ecx
mov cl, [kernel+0x1f1]
inc ecx
shl ecx, 9
rep movsb

; load the protected-mode kernel at 0x100000
mov edi, 0x100000
mov ecx, initrd
sub ecx, esi
rep movsb

; relocate the initrd (the address is taken from Minimal Linux Bootloader)
mov edi, 0x7fab000
mov esi, initrd
mov ecx, cmdline-initrd ; initrd size
rep movsb

; copy the command line
mov esi, cmdline
mov edi, 0x10000
cmdline_copy_loop:
lodsb
stosb
test al, al
jnz cmdline_copy_loop

; set up boot parameters
mov word [0x10210], 0x81ff ; bootloader ID & boot flags
mov word [0x10224], 0xde00 ; heap_end_ptr
mov byte [0x10227], 0x01 ; extended boot loader ID
mov dword [0x10228], 0x10000 ; command-line pointer
mov dword [0x10218], 0x7fab000 ; initrd pointer
mov dword [0x1021c], cmdline-initrd ; initrd size

; back up int13, int15, int1e
; to int93, int95, int9e
mov eax, [0x13*4]
mov [0x93*4], eax
mov eax, [0x15*4]
mov [0x95*4], eax
mov eax, [0x1e*4]
mov [0x9e*4], eax

xor al, al

; switch to real mode
jmp 0x18:final_jump
final_jump:
use16

mov eax, cr0
and al, 0xfe
mov cr0, eax

mov ax, 0x1000
mov ds, ax
mov es, ax
mov fs, ax
mov gs, ax
mov ss, ax
mov sp, 0xe000

; jump to the kernel entry
jmp 0x1020:0

loading_msg:
db "Loading the kernel...", 0

main_body_end:
align 512
times 512 db 0xb2
